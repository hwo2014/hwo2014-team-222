import sys
import socket
import json

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.angle = 0
        self.throttle_val = .65
        self.throttle_max = .7
        self.throttle_min = .45
        self.lane = 0
        self.init_data = {}
        self.track_pieces = {}
        self.lanes = {}
        self.shortest_lane = 0
        self.curr_lane = 0
        self.start_lane = 0
        self.switched_lane = False
        self.acceleration_rate = 0
        self.decceleration_rate = 0

    def calc_lane_length(self):
        distances = []
        for lane in self.lanes:
            total = 0
            for piece in self.track_pieces:
                if 'length' in piece:
                    total += piece['length']
                elif 'radius' in piece:
                    total += (piece['radius'] - lane['distanceFromCenter']) * piece['angle']
            distances.append(total)
            print "lane %s length = %s" % (lane['index'], total)
        self.shortest_lane = distances.index(min(distances))
            

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)
    
    def change_lanes(self, direction):
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        print data
#        if (abs(data[0]['angle']) > 5) and (self.throttle_val >= self.throttle_min):
#            self.throttle_val -= 0.1 
#        elif self.throttle_val < self.throttle_max:
#            self.throttle_val += 0.05
#        print self.throttle_val
#        self.start_lane = data[0]['piecePosition']['lane']['startLaneIndex'] 
#        if self.curr_lane > self.shortest_lane:
        if not self.switched_lane: 
            self.switched_lane = True
            print "sent lane change"
            self.change_lanes('Right')
#            self.curr_lane += 1
#        elif self.curr_lane < self.shortest_lane:
#            self.change_lanes('Right')
#            self.curr_lane -= 1
#        else:
        self.throttle(self.throttle_val)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                if format(msg_type) == "gameInit":
                    self.init_data = data
                    self.track_pieces = self.init_data['race']['track']['pieces']
                    self.lanes = self.init_data['race']['track']['lanes']
                    self.calc_lane_length()
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
